import pjlsa


class LSAClientGSI(pjlsa.BaseLSAClient):
    def __init__(self) -> None:
        """Connect to the integration LSA server at GSI."""
        cfg_url = "http://vmla004.acc.gsi.de:58080/application/profile/int/"
        super().__init__(
            "gsi-int",
            {
                "csco.default.property.config.url": cfg_url,
                "log4j.configurationFile": "log4j2-int.xml",
            },
        )
