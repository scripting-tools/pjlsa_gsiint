# GSI extension for pjlsa for GSI INT server

This pjlsa extension provides access to the GSI control systems on the integration server.

The Python module pjlsa (developed by CERN) provides access to the LSA API.

See also [the GSI-hosted pjlsa version](https://git.gsi.de/scripting-tools/pjlsa) for further information on pjlsa.

## Use

A simple example is provided in `example.py`. It prints all available patterns:

```python
import pjlsa_gsiint

with pjlsa_gsiint.LSAClientGSI().java_api():
    from cern.lsa.client import ServiceLocator, ContextService

contextService = ServiceLocator.getService(ContextService)
patterns = contextService.findPatterns()

for pattern in patterns:
    print (pattern)

```

Running this script within ASL, the accelerator services cluster, would give a typical output:

```
BUILD SUCCESSFUL in 1s
1 actionable task: 1 executed

[... INFO and DEBUG messages from LSA running on GSI integration server]

SCRATCH_IK_YRT1_CRYRING_FAST_20220627_135739
SCRATCH_RM_SIS18_PYTHON_TEST_20220728_124253
SIS18_FAST_TE_ESR_20220615
SCRATCH_SC_CRYRING_FAST_20220615_154447
SCRATCH_HL_SIS18_FAST_HHD_20230130_104825
SIS100_PROTON
SCRATCH_RM_SIS18_SLOW_HFS_20230328_144401
SIS18_FAST_HHD_BOOSTER_20220615
SCRATCH_SC_ESR_FAST_20220615_142831
SCRATCH_IK_YRT1_20220627_140049
SCRATCH_IK_ESR_FAST_HTR_20220630_113958
```

## Installation

Please refer to the [BEPHY wiki][PythonPackagingGSI] to learn how to set up
your Python environment. Once it is set up, you can pull the package directly
from the [Gitlab package registry][]:

```shell-session
$ pip install pjlsa-gsiint
```

You can also install the package directly from Git:

```shell-session
$ python -m pip install git+https://git.gsi.de/scripting-tools/pjlsa_gsiint.git
```

or clone the repository and then install it from the local directory:

```shell-session
$ git clone https://git.gsi.de/scripting-tools/pjlsa_gsiint.git
$ cd pjlsa_gsiint
$ pip install .
```

Note that this package depends on `cmmn-build-manager` and `pjlsa`. If you've set up your environment correctly, they should be downloaded from the [Gitlab package registry][]. If not, you might have to download and install them separately first.

Note also that, upon your first call of the GSI LSA client, a couple of minutes may be spent on downloading the jars via gradle from the servers. 

[PythonPackagingGSI]: https://wiki.gsi.de/BEPHY/PythonPackaging
[Gitlab package registry]: https://git.gsi.de/groups/scripting-tools/-/packages/

## Troubleshooting

### “Initial heap size set to a larger value than the maximum heap size”

If you see the following error on the first creation of `LSAClientGSI`:

```
Error occurred during initialization of VM
Initial heap size set to a larger value than the maximum heap size
---------------------------------------------------------------------------
OSError                                   Traceback (most recent call last)
...
OSError: error executing gradle!
```

then the most likely cause are broken command-line parameters that are passed
to Java. Check the `$JAVA_OPTS` variable and either adjust it as necessary or
remove it entirely:

```shell-session
$ unset JAVA_OPTS
```
