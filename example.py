#!/usr/bin/env python3

import pjlsa_gsiint

with pjlsa_gsiint.LSAClientGSI().java_api():
    from cern.lsa.client import ContextService, ServiceLocator

context_service = ServiceLocator.getService(ContextService)

for pattern in context_service.findPatterns():
    print(pattern)
